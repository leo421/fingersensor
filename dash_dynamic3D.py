import dash
import struct
from dash import dcc
# import dash_core_components as dcc
from dash import html
# import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import numpy as np
import serial
import time

# 串口设置
serial_port = 'COM28'  # 请替换为你的串口号
baud_rate = 115200

# 3D模型参数
grid_size = 100
surface_height = 100
deformation_scale = 0.5

# 初始化串口
ser = serial.Serial(serial_port, baud_rate)

# 生成初始平面网格
x = np.arange(0-grid_size, grid_size, 1)
y = np.arange(0-grid_size, grid_size, 1)
X, Y = np.meshgrid(x, y)
Z = np.full((grid_size*2, grid_size*2), 0)
# Z = np.zeros_like(x)

# Dash app 初始化
app = dash.Dash(__name__)



figure = go.Figure(data=[go.Surface(x=X, y=Y, z=Z)])
figure.update_layout(title="实时变形", autosize=False,
                        width=1600, height=1000,
                        margin=dict(l=65, r=50, b=65, t=90),
                        scene=dict(zaxis=dict(range=[0,100]))
                        )

app.layout = html.Div(children=[
    html.H1(children="平面受力分析"),
    dcc.Graph(id='live-3d-graph', figure=figure),
    dcc.Interval(id='interval-component', interval=1000, n_intervals=0) # 100ms 更新一次
])

# 回调函数，更新图形
# @app.callback(Output('live-3d-graph', 'extendData'),
@app.callback(Output('live-3d-graph', 'figure'),
              [Input('interval-component', 'n_intervals')])
def update_graph(n):
    global Z  # 使用全局Z，保证每次更新都基于上次的结果

    # 从串口读取数据
    try:
        # data = ser.readline().decode().strip().split(',')
        # print(data)
        data = ser.read(16)
        data_bytes = [data[j:j+4] for j in range(0, 16, 4)]
        bbdata = [struct.unpack('i', x)[0] for x in data_bytes]
        print(bbdata)
        force = float(bbdata[0])
        x_coord = int(bbdata[1])
        y_coord = int(bbdata[2])
    except:
        force = 0
        x_coord = 0
        y_coord = 0

    # 计算变形
    si = 0
    sj = 0
    for i in range(grid_size):
        for j in range(grid_size):
            distance = np.sqrt((i - x_coord)**2 + (j - y_coord)**2)
            deformation = force * np.exp(-distance**2 / 5) * deformation_scale
            # Z[i, j] = surface_height - deformation
            Z[i, j] = deformation
            if si==0 and sj==0 and deformation>5:
                si = i
                sj = j
                print(deformation)
                print(Z[i,j])
    print(Z.tolist()[si][sj])

    # 创建 3D 图形
    figure = go.Figure(data=[go.Surface(x=X, y=Y, z=Z)])
    figure.update_layout(title="实时变形", autosize=False,
                         width=1600, height=1000,
                         margin=dict(l=65, r=50, b=65, t=90),
                         scene=dict(zaxis=dict(range=[0,100]))
                         )
    return figure
    # return [go.Surface(x=X, y=Y, z=Z)]

if __name__ == '__main__':

    app.run_server(debug=False)