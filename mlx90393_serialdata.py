import serial
import math
import datetime
import time
import matplotlib.pyplot as plt
from drawnow import drawnow

# 指定串口和波特率
serial_port = 'COM4'  # 串口号根据实际情况修改
baud_rate = 9600

# 打开串口
ser = serial.Serial(serial_port, baud_rate, timeout=1)

# 定义累计数据数量
num_data_to_accumulate = 10
data_count = 0

# 初始化累计变量
sum_x = 0
sum_y = 0
sum_z = 0
sum_t = 0

d = ser.readline()
print(d)
d = ser.readline()
print(d)


# 绘图数据
data_x = []
data_y = []
data_z = []
data_t = []

# 绘图函数
def plot_data():
    # plt.plot(data_x, label="x", color='red')
    # plt.plot(data_y, label="y", color='blue')
    plt.plot(data_z, label="z", color='brown')
    plt.plot(data_t, label="t", color='green')
    plt.grid(True)

datalist = []
while True:
    datalist.clear()
    data_count = 0
    sum_x = 0
    sum_y = 0
    sum_z = 0
    sum_t = 0    
    while data_count < num_data_to_accumulate:
        # 读取一行数据
        line = ser.readline().decode().strip()
        # print("line=%s" % line)
        if len(line)==0:
            continue
        # 解析数据
        data = line.split()
        x = int(data[1].split('=')[1])
        y = int(data[2].split('=')[1])
        z = int(data[3].split('=')[1])
        t = int(data[4].split('=')[1])
        xr = int(data[5].split('=')[1])
        yr = int(data[6].split('=')[1])
        zr = int(data[7].split('=')[1])

        dn = datetime.datetime.now()
        dt = dn.strftime(r"%Y%m%d%H:%M:%S.%f")

        datalist.append((x,y,z,xr,yr,zr,t,dt))

        # 更新画图数据
        data_x.append(xr)
        data_y.append(yr)
        data_z.append(zr)
        data_t.append(t/4.01)
        # 更新图形    
        drawnow(plot_data)
        
        # 累加数据
        sum_x += x
        sum_y += y
        sum_z += z
        sum_t += t

        # 增加数据计数
        data_count += 1
        
        print(f"Read data {data_count}/{num_data_to_accumulate}: x={x}, xr={xr}, y={y}, yr={yr}, z={z}, zr={zr}, t={t}")
        time.sleep(0.1)  # 等待一段时间再读取下一行数据


    # 计算平均值
    avg_x = 1.0 * sum_x / num_data_to_accumulate
    avg_y = 1.0 * sum_y / num_data_to_accumulate
    avg_z = 1.0 * sum_z / num_data_to_accumulate
    avg_t = 1.0 * sum_t / num_data_to_accumulate
    vsum = round(math.sqrt(math.pow(avg_x,2) + math.pow(avg_y,2)))
    
    print(f"\nAverage values: x={avg_x}, y={avg_y}, z={avg_z}, t={avg_t}, vsum={vsum}")

    with open("serialdata.txt", 'a') as f:
        for d in datalist:
            # dn = datetime.datetime.now()
            # dt = dn.strftime(r"%Y%m%d%H:%M:%S.%f")
            f.write(f"{d[7]},{d[0]},{d[1]},{d[2]},{d[3]},{d[4]},{d[5]},{d[6]}\n")