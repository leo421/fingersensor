# import os
import serial
import struct
import configparser
import win32pipe
import win32file
# import pywintypes
import subprocess
import numpy as np
import matplotlib.pyplot as plt
# from mpl_toolkits.mplot3d import Axes3D

"""
通过串口读取数据进行展示
"""

# 默认配置
gconf = {
    "COM_PORT": "COM12",
    "BAUD_RATE": 115200,
    "DATA_TYPE": "float",
    "RESET_DATA_COUNT ": 200,
    "ZERO_DATA_COUNT ": 300,
    "TEST_ZERO": True,
    "UPDATE_VIEW_INTERVAL": 50,
    "SMOOTH_DATA": True,
    "SMOOTH_FACTOR": 1.0,
    "DATA_ADJ": True,
    "SENSOR_WIDTH": 20,
    "SENSOR_HEIGHT": 20,
    "VIEW_WIDTH": 40,
    "VIEW_HEIGHT": 40,
    "ZLIM": 6,
    "PEAK_RADIUS_FACTOR": 1.0
}

# 零点数据
zero_data = []

# 监听串口，读取数据
def read_data_from_serial(ser):
    while True:

        # data = ser.read(16)
        # data_bytes = [data[j:j+4] for j in range(0, 16, 4)]
        # if gconf['DATA_TYPE'] == "float":
        #     bbdata = [struct.unpack('f', x)[0] for x in data_bytes]
        # else:
        #     bbdata = [struct.unpack('i', x)[0] for x in data_bytes]
        # yield [
        #         bbdata[0] - zero_data[0],
        #         bbdata[1] - zero_data[1],
        #         bbdata[2] - zero_data[2],
        #         bbdata[3] - zero_data[3]
        # ]
        data = ser.readline()
        bbdata = data.decode("ascii").split(",")
        if len(bbdata) != 4:
            continue
        yield [
                int(bbdata[0]) - zero_data[0],
                int(bbdata[1]) - zero_data[1],
                int(bbdata[2]) - zero_data[2],
                int(bbdata[3]) - zero_data[3]
        ]

# 平滑处理数据
def smooth_data(data, history, alpha=0.2):
    if len(history) == 0:
        history.append(data)
    else:
        smoothed_data = (1 - alpha) * np.array(history[-1]) + alpha * np.array(data)
        history.append(smoothed_data.tolist())
        history.pop(0)
    return history[-1]

# 计算重物位置坐标
def calculate_position(sensor_data, weight):
    if gconf['DATA_TYPE'] == "float":
        if weight == 0.0:
            return 0.0, 0.0
    else:
        if weight == 0:
            return 0.0, 0.0
    # posX = (sensor_data[0] + sensor_data[1] - sensor_data[2] - sensor_data[3]) / 2
    # posY = (sensor_data[0] - sensor_data[1] + sensor_data[2] - sensor_data[3]) / 2
    posX = (0.0 - sensor_data[0] + sensor_data[1] - sensor_data[2] + sensor_data[3]) / 4.0 * gconf['SENSOR_WIDTH'] / 2 / weight
    posY = (0.0 + sensor_data[0] + sensor_data[1] - sensor_data[2] - sensor_data[3]) / 4.0 * gconf['SENSOR_HEIGHT'] / 2 / weight
    if posX > (gconf['VIEW_WIDTH'] / 2.0) or posX < 0 - (gconf['VIEW_WIDTH'] / 2.0):
        posX = 0.0
    if posY > (gconf['VIEW_HEIGHT'] / 2.0) or posY < 0 - (gconf['VIEW_HEIGHT'] / 2.0):
        posY = 0.0
    return posX, posY

# 可视化动态三维图形
def visualize_3d(data):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # x = np.linspace(-20, 20, 100)
    x = np.linspace(int(0 - gconf['VIEW_WIDTH']/2), int(gconf['VIEW_WIDTH']/2), int(gconf['VIEW_WIDTH'] * 100 / 40))
    # y = np.linspace(-20, 20, 100)
    y = np.linspace(int(0 - gconf['VIEW_HEIGHT']/2), int(gconf['VIEW_HEIGHT']/2), int(gconf['VIEW_HEIGHT'] * 100 / 40))
    X, Y = np.meshgrid(x, y)
    Z = np.zeros_like(X)

    posX, posY, weight = data
    if weight != 0:
        Z = weight * np.exp(-((X - posX)**2 + (Y - posY)**2) / (4 * weight * gconf['PEAK_RADIUS_FACTOR']))
    ax.plot_surface(
        X, 
        Y, 
        Z, 
        # cmap='viridis', 
        alpha=0.7
    )

    ax.text(posX, posY, weight, f'Force: {weight:.2f}\n({posX:.2f}, {posY:.2f})', color='red')  # 标注力大小和作用点坐标

    # 设置z轴固定比例
    if gconf['ZLIM'] > 0:
        ax.set_zlim(0, gconf['ZLIM'])

    plt.show(block=False)

    return fig, ax

# 更新动态图形
def update_3d(fig, ax, data):
    ax.clear()

    # x = np.linspace(-20, 20, 100)
    x = np.linspace(int(0 - gconf['VIEW_WIDTH']/2), int(gconf['VIEW_WIDTH']/2), int(gconf['VIEW_WIDTH'] * 100 / 40))
    # y = np.linspace(-20, 20, 100)
    y = np.linspace(int(0 - gconf['VIEW_HEIGHT']/2), int(gconf['VIEW_HEIGHT']/2), int(gconf['VIEW_HEIGHT'] * 100 / 40))
    X, Y = np.meshgrid(x, y)
    Z = np.zeros_like(X)

    posX, posY, weight = data
    # Z = weight *  np.exp(-((X - posX)**2 + (Y - posY)**2) / (2 * weight**2))
    if weight != 0:
        Z = weight * np.exp(-((X - posX)**2 + (Y - posY)**2) / (4 * weight * gconf['PEAK_RADIUS_FACTOR']))
    ax.plot_surface(
        X, 
        Y, 
        Z, 
        # cmap='viridis', 
        alpha=0.7
    )

    ax.text(posX, posY, weight, f'Force: {weight:.2f}\n({posX:.2f}, {posY:.2f})', color='red')  # 标注力大小和作用点坐标

    # 设置z轴固定比例
    if gconf['ZLIM'] > 0:
        ax.set_zlim(0, gconf['ZLIM'])

    fig.canvas.draw()
    fig.canvas.flush_events()

# 加载配置文件
def loadConfiguration():
    cp = configparser.ConfigParser()
    cp.read("pressure_view_serial.ini",encoding='utf-8')
    sec = cp['BASE']
    gconf['COM_PORT'] = sec.get("COM_PORT")
    gconf['BAUD_RATE'] = sec.getint('BAUD_RATE')
    gconf['DATA_TYPE'] = sec.get('DATA_TYPE')
    gconf['RESET_DATA_COUNT'] = sec.getint('RESET_DATA_COUNT')
    gconf['ZERO_DATA_COUNT'] = sec.getint('ZERO_DATA_COUNT')
    gconf['TEST_ZERO'] = sec.getboolean('TEST_ZERO')
    gconf['UPDATE_VIEW_INTERVAL'] = sec.getint('UPDATE_VIEW_INTERVAL')
    gconf['SMOOTH_DATA'] = sec.getboolean('SMOOTH_DATA')
    gconf['DATA_ADJ'] = sec.getboolean('DATA_ADJ')
    gconf['SMOOTH_FACTOR'] = sec.getfloat('SMOOTH_FACTOR')
    gconf['SENSOR_WIDTH'] = sec.getint('SENSOR_WIDTH')
    gconf['SENSOR_HEIGHT'] = sec.getint('SENSOR_HEIGHT')
    gconf['VIEW_WIDTH'] = sec.getint('VIEW_WIDTH')
    gconf['VIEW_HEIGHT'] = sec.getint('VIEW_HEIGHT')
    gconf['ZLIM'] = sec.getint('ZLIM')
    gconf['PEAK_RADIUS_FACTOR'] = sec.getfloat('PEAK_RADIUS_FACTOR')

if __name__ == "__main__":

    # 加载配置
    loadConfiguration()

    # 打开串口
    ser = serial.Serial(gconf['COM_PORT'], gconf['BAUD_RATE'])

    # 清零处理
    tl = 0.0
    tr = 0.0
    bl = 0.0
    br = 0.0
    for i in range(gconf['RESET_DATA_COUNT'] + gconf['ZERO_DATA_COUNT']):
        # data = ser.read(16)
        data = ser.readline()
        if i>=gconf['RESET_DATA_COUNT']:
            # data_bytes = [data[j:j+4] for j in range(0, 16, 4)]
            # if gconf['DATA_TYPE'] == "float":
            #     bbdata = [struct.unpack('f', x)[0] for x in data_bytes]
            # else:
            #     bbdata = [struct.unpack('i', x)[0] for x in data_bytes]
            bbdata = data.decode("ascii").split(",")
            tl += int(bbdata[0])
            tr += int(bbdata[1])
            bl += int(bbdata[2])
            br += int(bbdata[3])
    if gconf['TEST_ZERO']:
        # 测试数据
        if gconf['DATA_TYPE'] == "float":
            zero_data.append(0.0)
            zero_data.append(0.0)
            zero_data.append(0.0)
            zero_data.append(0.0)
        else:
            zero_data.append(0)
            zero_data.append(0)
            zero_data.append(0)
            zero_data.append(0)
    else:
        # 实际数据
        zero_data.append(tl / gconf['ZERO_DATA_COUNT'])
        zero_data.append(tr / gconf['ZERO_DATA_COUNT'])
        zero_data.append(bl / gconf['ZERO_DATA_COUNT'])
        zero_data.append(br / gconf['ZERO_DATA_COUNT'])

    history = []
    fig, ax = None, None
    i = 0
    for sensor_data in read_data_from_serial(ser):
        if gconf['SMOOTH_DATA']:
            smoothed_data = smooth_data(sensor_data, history, gconf['SMOOTH_FACTOR'])
        else:
            smooth_data = sensor_data
        total_weight = np.mean(smoothed_data)
        if gconf['DATA_ADJ']:
            # if total_weight < 0.001:
            #     total_weight = 0.001
            if gconf['DATA_TYPE'] == "float":
                if total_weight < 0.0:
                    total_weight = 0.0
            else:
                if total_weight < 0:
                    total_weight = 0
        posX, posY = calculate_position(sensor_data, total_weight)
        # result = (posX * 6.0, posY * 6.0, total_weight)
        result = (posX, posY, total_weight)

        if fig is None:
            fig, ax = visualize_3d(result)
        else:
            if i % gconf['UPDATE_VIEW_INTERVAL'] == 0:
                update_3d(fig, ax, result)
                i = 0
        i += 1