import serial
import math
import datetime
import time
import matplotlib.pyplot as plt
from drawnow import drawnow
import matplotlib.ticker as ticker
import numpy as np

"""
动态输出串口获取到的传感器数据，进行图形显示。
由于历史趋势数据量较大，当数据多时，更新图形需要的时间超过1秒，导致显示滞后。
如果数据较多，可以不显示历史趋势数据。
或者减少数据类型数量。
"""

# 指定串口和波特率
serial_port = 'COM4'  # 串口号根据实际情况修改
baud_rate = 9600

# 打开串口
ser = serial.Serial(serial_port, baud_rate, timeout=1)

# 定义累计数据数量
num_data_to_accumulate = 10
data_count = 0

# 初始化累计变量
sum_x = 0
sum_y = 0
sum_z = 0
sum_t = 0

d = ser.readline()
print(d)
d = ser.readline()
print(d)


# 绘图数据
data_x = []
data_y = []
data_xy = []
data_z = []
data_t = []

# 定义视图的显示，可以为1到5个元素，或者更多。
view_data = [
    ('X', data_x),
    ('Y', data_y),
    ('Z', data_z),
    ('|XY|', data_xy),
    ('T', data_t)
]
view_color = [
    'red',
    'green',
    'blue',
    'brown',
    'cyan'
]

fig, axes = plt.subplots(nrows=2, ncols=len(view_data))

# axes[0, 0].plot([5,5.2,5.5,5.3])
# axes[0, 1].plot(5, 10)
# axes[1, 0].plot(5, 10)
# axes[1, 1].plot(5, 10)

# ax_x = axes[0,0]
# ax_y = axes[0,1]
# ax_z = axes[1,0]
# ax_t = axes[1,1]
# plt.tight_layout()
# plt.legend()
for i, ax in enumerate(axes.flat):
    ax.grid(True)
    ax.legend()
    ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.0f'))

plt.show(block=False)


# 绘图函数
def plot_data():
    for i in range(len(view_data)):
        # d, = axes[0,i].plot(view_data[i][1], label=view_data[i][0], color=view_color[i])
        # axes[0,i].legend(handles=[d], loc="upper right")
        axes[1,i].cla()
        axes[1,i].grid(True)
        axes[1,i].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.0f'))
        if len(view_data[i][1])>20:
            dr, = axes[1,i].plot(view_data[i][1][-20:], label=view_data[i][0] + "\n" + str(int(round(sum(view_data[i][1][-20:])/20, 0))))
        else:
            dr, = axes[1,i].plot(view_data[i][1], label=view_data[i][0] + "\n" + str(int(round(sum(view_data[i][1])/len(view_data[i][1]), 0))))
        axes[1,i].legend(handles=[dr], loc="upper right")
        


    # d_x, = ax_x.plot(data_x, label="x", color="red")
    # ax_x.legend(handles=[d_x], loc='upper right')
    # d_y, = ax_y.plot(data_y, label="y", color="green")
    # ax_y.legend(handles=[d_y], loc='upper right')
    # # d_z, = ax_z.plot(data_z, label="z", color="blue")
    # ax_z.cla()
    # ax_z.grid(True)
    # ax_z.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.0f'))
    # if len(data_x)>20:
    #     d_z, = ax_z.plot(data_x[-20:], label="z", color="blue")
    # else:
    #     d_z, = ax_z.plot(data_x, label="z", color="blue")
    # ax_z.legend(handles=[d_z], loc='upper right')
    # # d_t, = ax_t.plot(data_t, label="t", color="brown")
    # ax_t.cla()
    # ax_t.grid(True)
    # ax_t.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.0f'))
    # if len(data_y)>20:
    #     d_t, = ax_t.plot(data_y[-20:], label="t", color="brown")
    # else:
    #     d_t, = ax_t.plot(data_y, label="t", color="brown")
    # ax_t.legend(handles=[d_t], loc='upper right')

    # plt.tight_layout()
    # ax_x.legend()
    # ax_y.legend()
    # ax_z.legend()
    # ax_xy.legend()

    # ax_x.title="X"
    # ax_y.title="Y"
    # ax_z.title="Z"
    # ax_t.title="t"

    fig.canvas.draw()
    fig.canvas.flush_events()
    
datalist = []
while True:
    datalist.clear()
    data_count = 0
    sum_x = 0
    sum_y = 0
    sum_z = 0
    sum_t = 0    
    while data_count < num_data_to_accumulate:
        # 读取一行数据
        line = ser.readline().decode().strip()
        # print("line=%s" % line)
        if len(line)==0:
            continue
        # 解析数据
        data = line.split()
        x = int(data[1].split('=')[1])
        y = int(data[2].split('=')[1])
        z = int(data[3].split('=')[1])
        t = int(data[4].split('=')[1])
        xr = int(data[5].split('=')[1])
        yr = int(data[6].split('=')[1])
        zr = int(data[7].split('=')[1])

        dn = datetime.datetime.now()
        dt = dn.strftime(r"%Y%m%d%H:%M:%S.%f")

        datalist.append((x,y,z,xr,yr,zr,t,dt))

        # 更新画图数据
        data_x.append(xr)
        data_y.append(yr)
        data_xy.append(math.sqrt(pow(xr,2) + pow(yr,2)))
        data_z.append(zr)
        data_t.append(t/4.01)

        # 更新图形    
        # drawnow(plot_data)
        print(datetime.datetime.now())
        plot_data()
        print(datetime.datetime.now())
        
        # 累加数据
        sum_x += x
        sum_y += y
        sum_z += z
        sum_t += t

        # 增加数据计数
        data_count += 1
        
        print(f"Read data {data_count}/{num_data_to_accumulate}: x={x}, xr={xr}, y={y}, yr={yr}, z={z}, zr={zr}, t={t}")
        # time.sleep(0.1)  # 等待一段时间再读取下一行数据


    # 计算平均值
    avg_x = 1.0 * sum_x / num_data_to_accumulate
    avg_y = 1.0 * sum_y / num_data_to_accumulate
    avg_z = 1.0 * sum_z / num_data_to_accumulate
    avg_t = 1.0 * sum_t / num_data_to_accumulate
    vsum = round(math.sqrt(math.pow(avg_x,2) + math.pow(avg_y,2)))
    
    print(f"\nAverage values: x={avg_x}, y={avg_y}, z={avg_z}, t={avg_t}, vsum={vsum}")

    with open("serialdata.txt", 'a') as f:
        for d in datalist:
            # dn = datetime.datetime.now()
            # dt = dn.strftime(r"%Y%m%d%H:%M:%S.%f")
            f.write(f"{d[7]},{d[0]},{d[1]},{d[2]},{d[3]},{d[4]},{d[5]},{d[6]}\n")