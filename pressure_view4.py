import os
import struct
import win32pipe
import win32file
import pywintypes
import subprocess
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# 获取传感器数据的管道名称
PIPE_NAME = r'\\.\pipe\WiiBB'
# 读取BalanceBoard工具程序的路径
WIIMOTE_PATH = r".\tools\WiimoteTest.exe"

# 零点数据
zero_data = []

# 监听命名管道，读取数据
def read_data_from_pipe(pipe_path):
    # with open(pipe_path, 'r') as pipe:
        while True:
            # data = pipe.readline().strip()
            # if not data:
            #     continue

            data = win32file.ReadFile(pipe_handle, 24)
            # print("收到的数据:", struct.unpack('f', data[1])[0])
            float_bytes = [data[1][i:i+4] for i in range(0, 24, 4)]
            bbdata = [struct.unpack('f', x)[0] for x in float_bytes]
            # yield list(bbdata)
            yield [
                  bbdata[0] - zero_data[0],
                  bbdata[1] - zero_data[1],
                  bbdata[2] - zero_data[2],
                  bbdata[3] - zero_data[3],
                  bbdata[4] - zero_data[4],
                  bbdata[5] - zero_data[5]
            ]

            # yield list(map(float, data.split(',')))

# 平滑处理数据
def smooth_data(data, history, alpha=0.2):
    if len(history) == 0:
        history.append(data)
    else:
        smoothed_data = (1 - alpha) * np.array(history[-1]) + alpha * np.array(data)
        history.append(smoothed_data.tolist())
    return history[-1]

# 计算重物位置坐标
def calculate_position(sensor_data, weight):
    # posX = (sensor_data[0] + sensor_data[1] - sensor_data[2] - sensor_data[3]) / 2
    # posY = (sensor_data[0] - sensor_data[1] + sensor_data[2] - sensor_data[3]) / 2
    posX = (0.0 - sensor_data[0] + sensor_data[1] - sensor_data[2] + sensor_data[3]) / 4.0 * 10 / weight
    posY = (0.0 + sensor_data[0] + sensor_data[1] - sensor_data[2] - sensor_data[3]) / 4.0 * 10 / weight
    if posX > 20.0 or posX < -20.0:
        posX = 0.0
    if posY > 20.0 or posY < -20.0:
        posY = 0.0
    return posX, posY

# 可视化动态三维图形
def visualize_3d(data):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = np.linspace(-20, 20, 100)
    y = np.linspace(-20, 20, 100)
    X, Y = np.meshgrid(x, y)
    Z = np.zeros_like(X)

    posX, posY, weight = data
    Z = weight * np.exp(-((X - posX)**2 + (Y - posY)**2) / (2 * weight**2))
    ax.plot_surface(
        X, 
        Y, 
        Z, 
        # cmap='viridis', 
        alpha=0.7
    )

    ax.text(posX, posY, weight*0.1, f'Force: {weight:.2f}\n({posX:.2f}, {posY:.2f})', color='red')  # 标注力大小和作用点坐标

    # 设置z轴固定比例
    ax.set_zlim(0, 6)

    plt.show(block=False)

    return fig, ax

# 更新动态图形
def update_3d(fig, ax, data):
    ax.clear()

    x = np.linspace(-20, 20, 100)
    y = np.linspace(-20, 20, 100)
    X, Y = np.meshgrid(x, y)
    Z = np.zeros_like(X)

    posX, posY, weight = data
    # Z = weight *  np.exp(-((X - posX)**2 + (Y - posY)**2) / (2 * weight**2))
    Z = weight *  np.exp(-((X - posX)**2 + (Y - posY)**2) / (4 * weight))
    ax.plot_surface(
        X, 
        Y, 
        Z, 
        # cmap='viridis', 
        alpha=0.7
    )

    ax.text(posX, posY, weight*0.1, f'Force: {weight:.2f}\n({posX:.2f}, {posY:.2f})', color='red')  # 标注力大小和作用点坐标

    ax.set_zlim(0, 6)

    fig.canvas.draw()
    plt.pause(0.01)

if __name__ == "__main__":
    # pipe_path = r'\\.\pipe\my_pipe'  # 命名管道路径，根据实际情况修改

    # 创建命名管道
    pipe_handle = win32pipe.CreateNamedPipe(
        PIPE_NAME,
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
        1,  # 最大实例数
        65536,  # 输出缓冲区大小
        65536,  # 输入缓冲区大小
        0,  # 默认超时时间
        None  # 默认安全属性
    )

    print("命名管道已创建，等待连接...")

    # 创建进程读取BalanceBoard数据
    subprocess.Popen(WIIMOTE_PATH)

    # 等待客户端连接
    win32pipe.ConnectNamedPipe(pipe_handle, None)

    print("已连接客户端。")

    # 清零处理
    tl = 0.0
    tr = 0.0
    bl = 0.0
    br = 0.0
    for i in range(500):
        data = win32file.ReadFile(pipe_handle, 24)
        if i>=200:
            float_bytes = [data[1][j:j+4] for j in range(0, 24, 4)]
            bbdata = [struct.unpack('f', x)[0] for x in float_bytes]
            tl += bbdata[0]
            tr += bbdata[1]
            bl += bbdata[2]
            br += bbdata[3]
    zero_data.append(tl / 300.0)
    zero_data.append(tr / 300.0)
    zero_data.append(bl / 300.0)
    zero_data.append(br / 300.0)
    zero_data.append(0.0)
    zero_data.append(0.0)


    history = []
    fig, ax = None, None
    i = 0
    for sensor_data in read_data_from_pipe(pipe_handle):
        smoothed_data = smooth_data(sensor_data[:4], history)
        total_weight = np.mean(smoothed_data)
        if total_weight < 0.001:
            total_weight = 0.001
        posX, posY = calculate_position(sensor_data[:4], total_weight)
        # result = (posX * 6.0, posY * 6.0, total_weight)
        result = (posX, posY, total_weight)

        if fig is None:
            fig, ax = visualize_3d(result)
        else:
            if i % 50 == 0:
                update_3d(fig, ax, result)
                i = 0
        i += 1