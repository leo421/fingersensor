import os
import struct
import win32pipe
import win32file
import pywintypes
import subprocess
import numpy as np
import matplotlib.pyplot as plt

# 获取传感器数据的管道名称
PIPE_NAME = r'\\.\pipe\WiiBB'
# 读取BalanceBoard工具程序的路径
WIIMOTE_PATH = r".\tools\WiimoteTest.exe"

# 监听命名管道，读取数据
def read_data_from_pipe(pipe_path):
    # with open(pipe_path, 'r') as pipe:
        while True:
            # data = pipe.readline().strip()
            # if not data:
            #     continue

            data = win32file.ReadFile(pipe_handle, 24)
            # print("收到的数据:", struct.unpack('f', data[1])[0])
            float_bytes = [data[1][i:i+4] for i in range(0, 24, 4)]
            bbdata = [struct.unpack('f', x)[0] for x in float_bytes]
            yield list(bbdata)

            # yield list(map(float, data.split(',')))

# 平滑处理数据
def smooth_data(data, history, alpha=0.2):
    if len(history) == 0:
        history.append(data)
    else:
        smoothed_data = (1 - alpha) * np.array(history[-1]) + alpha * np.array(data)
        history.append(smoothed_data.tolist())
    return history[-1]

# 计算重物位置坐标
def calculate_position(sensor_data):
    # 假设传感器数据格式为 [sensor1, sensor2, sensor3, sensor4, posX, posY]
    posX = (sensor_data[0] + sensor_data[1] - sensor_data[2] - sensor_data[3]) / 2
    posY = (sensor_data[0] - sensor_data[1] + sensor_data[2] - sensor_data[3]) / 2
    return posX, posY

# 可视化三维图形
def visualize_3d(data):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # 生成平面
    x = np.linspace(-10, 10, 100)
    y = np.linspace(-10, 10, 100)
    X, Y = np.meshgrid(x, y)
    Z = np.zeros_like(X)
    ax.plot_surface(X, Y, Z, alpha=0.5)

    # 生成顶起的高度
    posX, posY, weight = data
    Z = np.zeros_like(X) + weight * 0.1  # 这里的 0.1 是随意设定的比例系数，根据实际情况调整
    ax.plot_surface(X + posX, Y + posY, Z, alpha=0.7)

    plt.show()

if __name__ == "__main__":
    # pipe_path = r'\\.\pipe\WiiBB'  # 命名管道路径，根据实际情况修改

    # 创建命名管道
    pipe_handle = win32pipe.CreateNamedPipe(
        PIPE_NAME,
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
        1,  # 最大实例数
        65536,  # 输出缓冲区大小
        65536,  # 输入缓冲区大小
        0,  # 默认超时时间
        None  # 默认安全属性
    )

    print("命名管道已创建，等待连接...")

    # 创建进程读取BalanceBoard数据
    subprocess.Popen(WIIMOTE_PATH)

    # 等待客户端连接
    win32pipe.ConnectNamedPipe(pipe_handle, None)

    print("已连接客户端。")


    history = []

    for sensor_data in read_data_from_pipe(pipe_handle):
        smoothed_data = smooth_data(sensor_data[:4], history)
        total_weight = np.mean(smoothed_data)
        posX, posY = calculate_position(sensor_data[:4])
        result = (posX, posY, total_weight)
        visualize_3d(result)
