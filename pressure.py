import time
import struct
import win32pipe
import win32file
import pywintypes
import subprocess

# 获取传感器数据的管道名称
PIPE_NAME = r'\\.\pipe\WiiBB'
# 读取BalanceBoard工具程序的路径
WIIMOTE_PATH = r".\tools\WiimoteTest.exe"

history_data:list = []
# 用100组数据作为平滑处理，输出结果
def smoothData(bbData:list) -> list:
    if len(history_data)>=100:
        history_data.pop(0)
    history_data.append(bbData)
    r = [0.0,0.0,0.0,0.0,0.0,0.0]
    for i in range(0, len(history_data)):
        for j in range(0,6):
            r[j] += history_data[i][j]
    for j in range(0,6):
        r[j] = r[j]/len(history_data)
    return r

if __name__ == "__main__":

    # 创建命名管道
    pipe_handle = win32pipe.CreateNamedPipe(
        PIPE_NAME,
        win32pipe.PIPE_ACCESS_DUPLEX,
        win32pipe.PIPE_TYPE_MESSAGE | win32pipe.PIPE_READMODE_MESSAGE | win32pipe.PIPE_WAIT,
        1,  # 最大实例数
        65536,  # 输出缓冲区大小
        65536,  # 输入缓冲区大小
        0,  # 默认超时时间
        None  # 默认安全属性
    )

    print("命名管道已创建，等待连接...")

    # 创建进程读取BalanceBoard数据
    subprocess.Popen(WIIMOTE_PATH)

    # 等待客户端连接
    win32pipe.ConnectNamedPipe(pipe_handle, None)

    print("已连接客户端。")

    i = 0
    zero = 0.0
    zero_count = 0
    reset = False
    while True:
        # 从管道中读取数据
        try:
            # data = win32file.ReadFile(pipe_handle, 4096)
            data = win32file.ReadFile(pipe_handle, 24)
            # print("收到的数据:", struct.unpack('f', data[1])[0])
            float_bytes = [data[1][i:i+4] for i in range(0, 24, 4)]
            bbdata = [struct.unpack('f', x)[0] for x in float_bytes]
            # print(bbdata)
            # print(smoothData(bbdata))
            r = smoothData(bbdata)
            i += 1
            if i % 50 == 0:
                if not reset:
                    zero_count += 1
                    if zero_count >= 5:
                        zero = (r[0]+r[1]+r[2]+r[3]) / 4
                        reset = True
                else:
                    print(((r[0]+r[1]+r[2]+r[3])/4 - zero)*1000)
        except pywintypes.error as e:
            print("读取数据时出错:", e)
            break
        # time.sleep(0.1)

    # 关闭管道
    win32file.CloseHandle(pipe_handle)
