import serial
import struct
import random
import time

"""
模拟传感器设备，通过串口发送传感器数据，包括切向力数据。
每组数据48个字节，每4个字节一个数值，共12个数值，依此定义如下：
Zlt：左上传感器Z轴压力。
Zrt：右上传感器Z轴压力。
Zlb：左下传感器Z轴压力。
Zrb：右下传感器Z轴压力。
Xlt：左上传感器X轴切向力。
Xrt：右上传感器X轴切向力。
Xlb：左下传感器X轴切向力。
Xrb：右下传感器X轴切向力。
Ylt：左上传感器Y轴切向力。
Yrt：右上传感器Y轴切向力。
Ylb：左下传感器Y轴切向力。
Yrb：右下传感器Y轴切向力。
"""

# 串口配置
COM_PORT = 'COM11'  # 串口号
BAUD_RATE = 115200  # 波特率

def generate_random_numbers():
    # 生成4个随机数，范围从0.0000到2.0000
    random_numbers = [round(random.uniform(0, 12), 4) for _ in range(12)]
    return random_numbers

def generate_random_int():
    # 生成4个随机数，范围从0.0000到2.0000
    random_numbers = [int(round(random.uniform(0, 2000), 0)) for _ in range(4)]
    random_numbers.extend([int(round(random.uniform(-2000, 2000), 0)) for _ in range(8)])
    return random_numbers

# 打开串口
ser = serial.Serial(COM_PORT, BAUD_RATE)

try:
    while True:

        # 生成浮点数
        # data = struct.pack('4f', *generate_random_numbers())

        # 生成整数
        data = struct.pack('12i', *generate_random_int())

        ser.write(data)
        time.sleep(0.001)
        
except KeyboardInterrupt:
    # 如果用户按下了Ctrl+C，则关闭串口并退出程序
    ser.close()
    print("Program terminated by user.")
