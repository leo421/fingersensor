import serial
import struct
import random
import time

"""
模拟传感器设备，通过串口发送传感器数据
"""

# 串口配置
COM_PORT = 'COM10'  # 串口号
BAUD_RATE = 115200  # 波特率

def generate_random_numbers():
    # 生成4个随机数，范围从0.0000到2.0000
    random_numbers = [round(random.uniform(0, 12), 4) for _ in range(4)]
    return random_numbers

def generate_random_int():
    # 生成4个随机数，范围从0.0000到2.0000
    random_numbers = [int(round(random.uniform(0, 100), 4)) for _ in range(4)]
    return random_numbers

# 打开串口
ser = serial.Serial(COM_PORT, BAUD_RATE)

try:
    while True:

        # 生成浮点数
        # data = struct.pack('4f', *generate_random_numbers())

        # 生成整数
        data = struct.pack('4i', *generate_random_int())

        ser.write(data)
        time.sleep(0.001)
        
except KeyboardInterrupt:
    # 如果用户按下了Ctrl+C，则关闭串口并退出程序
    ser.close()
    print("Program terminated by user.")
